# -*- coding: utf-8 -*-
import logging
import logging.config
import globVar
import json
import os
import shutil
import xml.etree.ElementTree as ET
import gzip
import random
import datetime

SuccEpsAttachRate_Max = 0.995
SuccEpsAttachRate_Min = 0.985

PagingSuccRate_Max = 0.995
PagingSuccRate_Min = 0.985

TauUpdateRate_Max = 0.995
TauUpdateRate_Min = 0.985

try:
    logging.config.fileConfig(globVar.LOGGER_FILE)
    logger = logging.getLogger("beautify")
except Exception, e:
    print(e)

INPUT_DIR = globVar.INPUT_DIR
OUTPUT_DIR = globVar.OUTPUT_DIR
WORK_DIR = globVar.WORK_DIR
FACTORS_FILE = globVar.FACTORS_FILE

"""
<nbi@gzltenbm01> </opt/nbi/DataFile/GD/HX/ER/GZ_OMC1/PM/2019021209> ls -lrth | grep MME | grep -v R1
-rw-r--r-- 1 nbi nbi 1.4M Feb 12 09:26 PM-MME-A1-V3.0.0-20190212090000-15.xml.gz
-rw-r--r-- 1 nbi nbi 1.4M Feb 12 09:41 PM-MME-A1-V3.0.0-20190212091500-15.xml.gz
-rw-r--r-- 1 nbi nbi 1.4M Feb 12 09:56 PM-MME-A1-V3.0.0-20190212093000-15.xml.gz
-rw-r--r-- 1 nbi nbi 1.4M Feb 12 10:11 PM-MME-A1-V3.0.0-20190212094500-15.xml.gz
cron:
12,27,42,57 * * * * python /opt/nbi/DataFile/pqmme/bin/beautify.py > /dev/null

12:24 PM-MME-A1-V3.0.0-20190510120000-15.xml.gz
12:39 PM-MME-A1-V3.0.0-20190510121500-15.xml.gz
12:54 PM-MME-A1-V3.0.0-20190510123000-15.xml.gz
13:14 PM-MME-A1-V3.0.0-20190510124500-15.xml.gz
cron: 15,25,40,55 * * * * python /opt/nbi/DataFile/pqmme/bin/beautify.py > /dev/null
"""

def sumList(list, factor=1.000):
    sum = 0
    for ele in list:
        sum = sum + int(ele * factor)
    return sum

def beatify():
    logger.info("START")

    # Find MME PM files
    filePathList = []
    now = datetime.datetime.now()
    cur_day = now.strftime("%d")
    cur_date = now.strftime("%Y%m") + cur_day
    cur_min = now.strftime("%M")
    cur_hour = now.strftime("%H")
    cur_sec = "00"
    filename_min = cur_min
    if cur_min == "25":
        filename_min = "00"
    elif cur_min == "40":
        filename_min = "15"
    elif cur_min == "55":
        filename_min = "30"
    elif cur_min == "15":
        filename_min = "45"
    else:
        pass

    filename_hour = cur_hour
    folder_hour = cur_hour
    delta = datetime.timedelta(hours=-1)
    if cur_min == "15":
        filename_hour = (now + delta).strftime("%H")
        folder_hour = (now + delta).strftime("%H")
        if cur_hour == "00":
            delta_day = datetime.timedelta(days=-1)
            cur_day = (now + delta_day).strftime("%d")
            cur_date = now.strftime("%Y%m") + cur_day
    else:
        pass
    
    directory = "/opt/nbi/DataFile/GD/HX/ER/GZ_OMC1/PM/" + cur_date + folder_hour
    filename = "PM-MME-A1-V3.0.0-" + cur_date + filename_hour + filename_min + cur_sec + "-15.xml.gz"
    file = os.path.join(directory, filename)

    if os.path.isfile(file):
        logger.info("found {file}".format(file=file))
        timeDirName = cur_date + folder_hour
        workDirWithTime = os.path.join(WORK_DIR, timeDirName)
        if not os.path.isdir(workDirWithTime):
            os.mkdir(workDirWithTime)
            logger.info("create directory {dir}".format(dir=workDirWithTime))
        workFile = os.path.join(workDirWithTime, filename)
        # For Test, copy
        #shutil.copy(file, workFile)
        # For Real
        shutil.move(file, workFile)
        filePathList.append(workFile)
        logger.info("moved {sfile} to {dfile}".format(sfile=file, dfile=workFile))
    else:
        logger.info("missing {file}".format(file=file))
        
    logger.info("ready to process MME PM file: {pathList}".format(pathList=filePathList))

    # Load factors.json
    factorsDict = {}
    with open(FACTORS_FILE, "r") as f:
        factorsDict = json.load(f)
    logger.debug("factors list: {f}".format(f=factorsDict))
    taList = factorsDict.keys()
    logger.debug("TA list: {ta}".format(ta=taList))

    # counters for compute SuccEpsAttachRate, PagingSuccRate, TauUpdateRate
    countersForComputeList = ["MM.SuccEpsAttach._Ta", "MM.AttEpsAttach._Ta", "MM.FailedEpsAttach._Ta.7.User", "MM.FailedEpsAttach._Ta.15.User", \
                              "MM.FailedEpsAttach._Ta.19.User", "MM.FirstPagingSucc._Ta","MM.SecondPagingSucc._Ta", "MM.PagAtt._Ta", \
                              "MM.TauAccept._Ta", "MM.TauRequest._Ta"]

    # counter name in factor.json mapping to CV N name
    counterCVNFirstHalfMappingDict = {"MM.SuccEpsAttach._Ta":"MM.SuccEpsAttach.",\
                                     "MM.AttEpsAttach._Ta":"MM.AttEpsAttach.", \
                                     "MM.FailedEpsAttach._Ta.7.User": "MM.FailedEpsAttach.", \
                                     "MM.FailedEpsAttach._Ta.15.User": "MM.FailedEpsAttach.", \
                                     "MM.FailedEpsAttach._Ta.19.User":"MM.FailedEpsAttach.", \
                                     "MM.FirstPagingSucc._Ta": "MM.FirstPagingSucc.", \
                                     "MM.SecondPagingSucc._Ta": "MM.SecondPagingSucc.", \
                                     "MM.PagAtt._Ta": "MM.PagAtt.", \
                                     "MM.TauAccept._Ta": "MM.TauAccept.", \
                                     "MM.TauRequest._Ta": "MM.TauRequest.", \
                                     }

    # Process
    for file in filePathList:
        logger.info("start processing {file}".format(file=file))

        """
        # data var for recording whole detai counter data
        Example
        data = {
            "MM.SuccEpsAttach.4600009052": [1,2,3,4,5,6],
            "MM.FailedEpsAttach.4600009052": [2,3,4,5,6,7,8,9],
        }
        """
        data = {}

        fh = gzip.open(file, "r")
        try:
            content = fh.read()
        except Exception, e:
            logger.error(e)
        finally:
            fh.close()
        oldFile = file.replace(".gz", ".old")
        with open(oldFile, "w") as f:
            f.write(content)
        logger.info("generated {file}".format(file=oldFile))

        tree = ET.parse(oldFile)
        root = tree.getroot()
        logger.info("parsed {file}".format(file=oldFile))

        for ta in taList:
            logger.info("TA: {ta}".format(ta=ta))
            counters = factorsDict[ta][0].keys()
            counterIndexMappingDict = {}

            # Get Counter index number
            for N in root.getiterator("N"):
                if N.text in counters or N.text in countersForComputeList:
                    index = N.attrib["i"]
                    counterIndexMappingDict[N.text] = index
                    #logger.info("found counter: {counter}, index: {index}".format(counter=N.text, index=index))
            logger.info("counter index mapping: {map}".format(map=counterIndexMappingDict))

            for i in range(len(root)):
                Measurements = root[i]
                for j in range(len(Measurements)):
                    if root[i][j].tag == "ObjectType" and root[i][j].text == "MmeFunction":
                        MeasurementsLoc = i
                        logger.info("found ObjectType: {ot}, location: {loc}".format(ot=root[i][j].text, loc=MeasurementsLoc))
                        break
            for i in range(len(root[MeasurementsLoc])):
                if root[MeasurementsLoc][i].tag == "PmData":
                    PmDataLoc = i
                    logger.info("found PmData location: {loc}".format(loc=PmDataLoc))
                    break
            ObjectNum = len(root[MeasurementsLoc][PmDataLoc])
            logger.info("found total MME: {num}".format(num=ObjectNum))

            # generate data {}
            countersForComputeSumDict = {}
            if not ta in countersForComputeSumDict.keys():
                countersForComputeSumDict[ta] = {}
            for counter in countersForComputeList:
                if counter not in countersForComputeSumDict[ta].keys():
                    countersForComputeSumDict[ta][counter] = 0
                sum = countersForComputeSumDict[ta][counter]
                data[counter] = []
                for i in range(ObjectNum):
                    # Object
                    ObjectLoc = i
                    UserLabel = root[MeasurementsLoc][PmDataLoc][ObjectLoc].get("UserLabel")
                    for j in range(len(root[MeasurementsLoc][PmDataLoc][ObjectLoc])):
                        if root[MeasurementsLoc][PmDataLoc][ObjectLoc][j].get("i") == counterIndexMappingDict[counter]:
                            CVLoc = j
                            if counter in counterCVNFirstHalfMappingDict:
                                counterWithTAName = counterCVNFirstHalfMappingDict[counter] + ta
                                #logger.info("processing {counter} with TA: {cvn}".format(counter=counter, cvn=counterWithTAName))
                                for k in range(len(root[MeasurementsLoc][PmDataLoc][ObjectLoc][CVLoc])-1):
                                    SNSVLoc = k
                                    SN = root[MeasurementsLoc][PmDataLoc][ObjectLoc][CVLoc][SNSVLoc].text
                                    if SN == counterWithTAName:
                                        SV = int(root[MeasurementsLoc][PmDataLoc][ObjectLoc][CVLoc][SNSVLoc + 1].text)
                                        data[counter].append(SV)
                                        logger.debug("{ta},{mme} {counter} {sn} {sv}"\
                                                     .format(ta=ta, mme=UserLabel, counter=counter, sn=SN, sv=SV))
                                        """
                                        if counter in counters:
                                            fact = float(factorsDict[ta][0][counter])
                                        else:
                                            fact = 1.00
                                        if fact < 1 and SV == 1:   # Fix 1*0.75=0 then cause new rate < old rate, new value < old value
                                            SV_new = SV
                                        else:
                                            SV_new = int(SV * fact)
                                        old = sum
                                        sum = sum + SV_new
                                        countersForComputeSumDict[ta][counter] = sum
                                        logger.debug("{ta},{mme} {counter} {sn} {ovalue}+({sv}*{fact}={sv_new}) -> {nvalue}"\
                                                     .format(ta=ta, mme=UserLabel, counter=counter, sn=SN, ovalue=old, sv=SV, fact=fact, sv_new=SV_new, nvalue=sum))
                                                     """
                                        #logger.debug(data)
                                        #logger.info("found {mme} {counter} with value {value}".format(mme=UserLabel, counter=SN, value=SV))
                                    else:
                                        pass
                            else:
                                logger.error("{counter} Not in CV N First Half Mapping Directory".format(counter=counter))
                        else:
                            pass
            #logger.info("sum up directory: {dict}".format(dict=countersForComputeSumDict))

            # Caculate Original Rate and New Rate mutiply with factor
            try:
                # SuccEpsAttachRate
                #if countersForComputeSumDict[ta]["MM.SuccEpsAttach._Ta"] == 0 and countersForComputeSumDict[ta]["MM.AttEpsAttach._Ta"] == 0:
                if (sumList(data["MM.AttEpsAttach._Ta"]) - sumList(data["MM.FailedEpsAttach._Ta.7.User"]) - sumList(data["MM.FailedEpsAttach._Ta.15.User"]) - sumList(data["MM.FailedEpsAttach._Ta.19.User"])) == 0:
                    SuccEpsAttachRate = 0
                    logger.info("{ta} SuccEpsAttachRate: 0, due to {c1}-{c2}-{c3}-{c4} is 0".format(ta=ta, c1="MM.AttEpsAttach._Ta", c2="MM.FailedEpsAttach._Ta.7.User", c3="MM.FailedEpsAttach._Ta.15.User", c4="MM.FailedEpsAttach._Ta.19.User"))
                    SuccEpsAttachRate_New = 0
                else:
                    #SuccEpsAttachRate = float(countersForComputeSumDict[ta]["MM.SuccEpsAttach._Ta"]) / float(countersForComputeSumDict[ta]["MM.AttEpsAttach._Ta"] - countersForComputeSumDict[ta]["MM.FailedEpsAttach._Ta.7.User"] - countersForComputeSumDict[ta]["MM.FailedEpsAttach._Ta.15.User"] - countersForComputeSumDict[ta]["MM.FailedEpsAttach._Ta.19.User"])
                    SuccEpsAttachRate = float(sumList(data["MM.SuccEpsAttach._Ta"])) / float((sumList(data["MM.AttEpsAttach._Ta"]) - sumList(data["MM.FailedEpsAttach._Ta.7.User"]) - sumList(data["MM.FailedEpsAttach._Ta.15.User"]) - sumList(data["MM.FailedEpsAttach._Ta.19.User"])))
                    SuccEpsAttachRate_New = float(sumList(data["MM.SuccEpsAttach._Ta"], factorsDict[ta][0]["MM.SuccEpsAttach._Ta"])) / float((sumList(data["MM.AttEpsAttach._Ta"], factorsDict[ta][0]["MM.AttEpsAttach._Ta"]) - sumList(data["MM.FailedEpsAttach._Ta.7.User"]) - sumList(data["MM.FailedEpsAttach._Ta.15.User"]) - sumList(data["MM.FailedEpsAttach._Ta.19.User"])))
                    logger.info("{ta} original SuccEpsAttachRate: {orate}, with factors SuccEpsAttachRate: {nrate}".format(ta=ta, orate=SuccEpsAttachRate, nrate=SuccEpsAttachRate_New))
            except Exception, e:
                logger.error(e)
            try:
                # PagingSuccRate
                #if countersForComputeSumDict[ta]["MM.PagAtt._Ta"] == 0:
                if sumList(data["MM.PagAtt._Ta"]) == 0:
                    PagingSuccRate = 0
                    PagingSuccRate_New = 0
                    logger.info("{ta} PagingSuccRate: 0, due to {counter} is 0".format(ta=ta, counter="MM.PagAtt._Ta"))
                else:
                    #PagingSuccRate = float(countersForComputeSumDict[ta]["MM.FirstPagingSucc._Ta"] + countersForComputeSumDict[ta]["MM.SecondPagingSucc._Ta"]) / float(countersForComputeSumDict[ta]["MM.PagAtt._Ta"])
                    PagingSuccRate = float((sumList(data["MM.FirstPagingSucc._Ta"]) + sumList(data["MM.SecondPagingSucc._Ta"]))) / float(sumList(data["MM.PagAtt._Ta"]))
                    PagingSuccRate_New = float((sumList(data["MM.FirstPagingSucc._Ta"], factorsDict[ta][0]["MM.FirstPagingSucc._Ta"]) + sumList(data["MM.SecondPagingSucc._Ta"]))) / float(sumList(data["MM.PagAtt._Ta"], factorsDict[ta][0]["MM.PagAtt._Ta"]))
                    logger.info("{ta} original PagingSuccRate: {orate}, with factors PagingSuccRate: {nrate}".format(ta=ta, orate=PagingSuccRate, nrate=PagingSuccRate_New))
            except Exception, e:
                logger.error(e)
            try:
                # TauUpdateRate
                #if countersForComputeSumDict[ta]["MM.TauRequest._Ta"] == 0:
                if sumList(data["MM.TauRequest._Ta"]) == 0:
                    TauUpdateRate = 0
                    TauUpdateRate_New = 0
                    logger.info("{ta} TauUpdateRate: 0, due to {counter} is 0".format(ta=ta, counter="MM.TauRequest._Ta"))
                else:
                    #TauUpdateRate = float(countersForComputeSumDict[ta]["MM.TauAccept._Ta"]) / float(countersForComputeSumDict[ta]["MM.TauRequest._Ta"])
                    TauUpdateRate = float(sumList(data["MM.TauAccept._Ta"])) / float(sumList(data["MM.TauRequest._Ta"]))
                    TauUpdateRate_New = float(sumList(data["MM.TauAccept._Ta"], factorsDict[ta][0]["MM.TauAccept._Ta"])) / float(sumList(data["MM.TauRequest._Ta"]))
                    logger.info("{ta} original TauUpdateRate: {orate}, with factors TauUpdateRate: {nrate}".format(ta=ta, orate=TauUpdateRate, nrate=TauUpdateRate_New))
            except Exception, e:
                logger.error(e)

            # Adjust factor
            # SuccEpsAttachRate
            if SuccEpsAttachRate >= 0.985:
                factorsDict[ta][0]["MM.SuccEpsAttach._Ta"] = 1.000
                factorsDict[ta][0]["MM.AttEpsAttach._Ta"] = 1.000
                logger.info("{ta} SuccEpsAttachRate already >= 0.985, set MM.SuccEpsAttach._Ta and MM.AttEpsAttach._Ta factor to 1".format(ta=ta))
                #logger.debug("factors list change to: {f}".format(f=factorsDict[ta]))
            elif SuccEpsAttachRate_New< 0:
                factorsDict[ta][0]["MM.SuccEpsAttach._Ta"] = 1.000
                factorsDict[ta][0]["MM.AttEpsAttach._Ta"] = 1.000
                logger.info("{ta} SuccEpsAttachRate exception, set MM.SuccEpsAttach._Ta and MM.AttEpsAttach._Ta factor to 1".format(ta=ta))
                #logger.debug("factors list change to: {f}".format(f=factorsDict[ta]))
            elif SuccEpsAttachRate_New > 1:
                SuccEpsAttachRate_Target = random.uniform(SuccEpsAttachRate_Min, SuccEpsAttachRate_Max)
                SuccEpsAttachRate_TryTimes = 0
                while(SuccEpsAttachRate_New >= SuccEpsAttachRate_Target):
                    SuccEpsAttachRate_TryTimes = SuccEpsAttachRate_TryTimes + 1
                    factorsDict[ta][0]["MM.AttEpsAttach._Ta"] = factorsDict[ta][0]["MM.AttEpsAttach._Ta"] + 0.001
                    SuccEpsAttachRate_New = float(sumList(data["MM.SuccEpsAttach._Ta"],factorsDict[ta][0]["MM.SuccEpsAttach._Ta"])) / float((sumList(data["MM.AttEpsAttach._Ta"],factorsDict[ta][0]["MM.AttEpsAttach._Ta"]) - sumList(data["MM.FailedEpsAttach._Ta.7.User"]) - sumList(data["MM.FailedEpsAttach._Ta.15.User"]) - sumList(data["MM.FailedEpsAttach._Ta.19.User"])))
                logger.info("{ta} SuccEpsAttachRate > 1, set MM.AttEpsAttach._Ta factor to {fact}, SuccEpsAttachRate will be {r}, tried {times} times".format(ta=ta, fact=factorsDict[ta][0]["MM.AttEpsAttach._Ta"], r=SuccEpsAttachRate_New,times=SuccEpsAttachRate_TryTimes))
                #logger.debug("factors list change to: {f}".format(f=factorsDict[ta]))
            else:
                pass
            # PagingSuccRate
            if PagingSuccRate >= 0.985:
                factorsDict[ta][0]["MM.FirstPagingSucc._Ta"] = 1.000
                factorsDict[ta][0]["MM.PagAtt._Ta"] = 1.000
                logger.info("{ta} PagingSuccRate already >= 0.985, set MM.FirstPagingSucc._Ta and MM.PagAtt._Ta factor to 1".format(ta=ta))
                #logger.debug("factors list change to: {f}".format(f=factorsDict[ta]))
            elif PagingSuccRate_New < 0:
                factorsDict[ta][0]["MM.FirstPagingSucc._Ta"] = 1.000
                factorsDict[ta][0]["MM.PagAtt._Ta"] = 1.000
                logger.info("{ta} PagingSuccRate exception, set MM.FirstPagingSucc._Ta and MM.PagAtt._Ta factor to 1".format(ta=ta))
                #logger.debug("factors list change to: {f}".format(f=factorsDict[ta]))
            elif PagingSuccRate_New > 1:
                PagingSuccRate_Target = random.uniform(PagingSuccRate_Min, PagingSuccRate_Max)
                #logger.debug("set target {t}".format(t=PagingSuccRate_Target))
                PagingSuccRate_TryTimes = 0
                while(PagingSuccRate_New >=PagingSuccRate_Target):
                    PagingSuccRate_TryTimes = PagingSuccRate_TryTimes + 1
                    factorsDict[ta][0]["MM.PagAtt._Ta"] = factorsDict[ta][0]["MM.PagAtt._Ta"] + 0.001
                    PagingSuccRate_New = float((sumList(data["MM.FirstPagingSucc._Ta"], factorsDict[ta][0]["MM.FirstPagingSucc._Ta"]) + sumList(data["MM.SecondPagingSucc._Ta"]))) / float(sumList(data["MM.PagAtt._Ta"], factorsDict[ta][0]["MM.PagAtt._Ta"]))
                    #logger.info("ajust times: {times}, MM.PagAtt._Ta factor adjust to {f}, PagingSuccRate will be {r}".format(times=PagingSuccRate_TryTimes, f=factorsDict[ta][0]["MM.PagAtt._Ta"], r=PagingSuccRate_New))
                logger.info("{ta} PagingSuccRate > 1, set MM.FirstPagingSucc._Ta factor to {fact}, PagingSuccRate will be {r}, tried {times} times".format(ta=ta, fact=factorsDict[ta][0]["MM.PagAtt._Ta"], r=PagingSuccRate_New, times=PagingSuccRate_TryTimes))
                #logger.debug("factors list change to: {f}".format(f=factorsDict[ta]))
            else:
                pass
            # TauUpdateRate
            if TauUpdateRate >= 0.985:
                factorsDict[ta][0]["MM.TauAccept._Ta"] = 1
                logger.info("{ta} TauUpdateRate already >= 0.985, set MM.TauAccept._Ta factor to 1".format(ta=ta))
            elif  TauUpdateRate_New < 0:
                factorsDict[ta][0]["MM.TauAccept._Ta"] = 1
                logger.info("{ta} TauUpdateRate exception, set MM.TauAccept._Ta factor to 1".format(ta=ta))
                #logger.debug("factors list change to: {f}".format(f=factorsDict[ta]))
            elif TauUpdateRate_New > 1:
                TauUpdateRate_Target = random.uniform(TauUpdateRate_Min, TauUpdateRate_Max)
                TauUpdateRate_TryTimes = 0
                while(TauUpdateRate_New >= TauUpdateRate_Target):
                    TauUpdateRate_TryTimes = TauUpdateRate_TryTimes + 1
                    factorsDict[ta][0]["MM.TauAccept._Ta"] = factorsDict[ta][0]["MM.TauAccept._Ta"] - 0.001
                    TauUpdateRate_New = float(sumList(data["MM.TauAccept._Ta"],factorsDict[ta][0]["MM.TauAccept._Ta"])) / float(sumList(data["MM.TauRequest._Ta"]))
                logger.info("{ta} TauUpdateRate > 1, set MM.TauAccept._Ta factor to {fact}, TauUpdateRate will be {r}, tried {times} times".format(ta=ta, fact=factorsDict[ta][0]["MM.TauAccept._Ta"], r=TauUpdateRate_New,times=TauUpdateRate_TryTimes))
            else:
                pass
            logger.debug("factors list change to: {f}".format(f=factorsDict[ta]))

            # Modify
            for counter in counters:
                for i in range(ObjectNum):
                    # Object
                    ObjectLoc = i
                    UserLabel = root[MeasurementsLoc][PmDataLoc][ObjectLoc].get("UserLabel")
                    for j in range(len(root[MeasurementsLoc][PmDataLoc][ObjectLoc])):
                        if root[MeasurementsLoc][PmDataLoc][ObjectLoc][j].get("i") == counterIndexMappingDict[counter]:
                            CVLoc = j
                            if counter in counterCVNFirstHalfMappingDict:
                                counterWithTAName = counterCVNFirstHalfMappingDict[counter] + ta
                                #logger.info("processing {counter} with TA: {cvn}".format(counter=counter, cvn=counterWithTAName))
                                for k in range(len(root[MeasurementsLoc][PmDataLoc][ObjectLoc][CVLoc])-1):
                                    SNSVLoc = k
                                    SN = root[MeasurementsLoc][PmDataLoc][ObjectLoc][CVLoc][SNSVLoc].text
                                    if SN == counterWithTAName:
                                        SV = float(root[MeasurementsLoc][PmDataLoc][ObjectLoc][CVLoc][SNSVLoc+1].text)
                                        #logger.info("found {mme} {counter} with value {value}".format(mme=UserLabel, counter=SN, value=SV))
                                        fact = float(factorsDict[ta][0][counter])
                                        #if fact < 1 and SV == 1:   # Fix 1*0.75=0 then cause new rate < old rate, new value < old value
                                        #if SV == 1:
                                        if SV < 50:   # 2019.03.11 Fix if SV too small
                                            SV_new = int(SV)
                                        else:
                                            SV_new = int(SV * fact)
                                        root[MeasurementsLoc][PmDataLoc][ObjectLoc][CVLoc][SNSVLoc+1].text = str(SV_new)
                                        logger.info("{mme} {sn} {value}*{fact} -> {value_new}".format(mme=UserLabel, sn=SN, value=SV, fact=fact, value_new=SV_new))
                                    else:
                                        pass
                            else:
                                logger.error("{counter} Not in CV N First Half Mapping Directory".format(counter=counter))
                        else:
                            pass
                            

        newFile = file.replace(".gz",".new")
        tree.write(newFile, encoding="UTF-8")
        logger.info("write {file}".format(file=newFile))
        with open(newFile, "r") as f:
            content = f.readlines()
        for i in range(len(content)):
            if content[i].startswith("<PmFile"):
                content[i] = "<PmFile xmlns:xs=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">\n"
                logger.info("modified name space information")
                break
        with open(newFile, "w") as f:
            f.writelines(content)
        logger.info("generated {file}".format(file=newFile))

        with open(newFile, "r") as f:
            content = f.read()
        filename = os.path.basename(file)
        curdir = os.getcwd()
        finalOutDir = os.path.join(globVar.OUTPUT_DIR,file.split(os.path.sep)[-2])
        if not os.path.isdir(finalOutDir):
            os.mkdir(finalOutDir)
            logger.info("create directory {dir}".format(dir=finalOutDir))
        os.chdir(finalOutDir)
        fh = gzip.open(filename, 'w')
        try:
            fh.write(content)
            logger.info("generated {file}".format(file=os.path.join(finalOutDir, filename)))
        finally:
            os.chdir(curdir)
            fh.close()


    logger.info("END")

if __name__ == "__main__":
    beatify()