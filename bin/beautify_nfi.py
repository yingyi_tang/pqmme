# -*- coding: utf-8 -*-
import logging
import logging.config
import globVar
import json
import os
import shutil
import xml.etree.ElementTree as ET
import gzip
import random

SuccEpsAttachRate_Max = 0.9880
SuccEpsAttachRate_Min = 0.9800

PagingSuccRate_Max = 0.9880
PagingSuccRate_Min = 0.9800

TauUpdateRate_Max = 0.9880
TauUpdateRate_Min = 0.9800

try:
    logging.config.fileConfig(globVar.LOGGER_FILE)
    logger = logging.getLogger("beautify")
except Exception, e:
    print(e)

INPUT_DIR = globVar.INPUT_DIR
OUTPUT_DIR = globVar.OUTPUT_DIR
WORK_DIR = globVar.WORK_DIR
FACTORS_FILE = globVar.FACTORS_FILE

def beatify():
    logger.info("START")

    # Find MME PM files
    filePathList = []
    for dirpath, dirnames, filenames in os.walk(INPUT_DIR):
        for filename in filenames:
            timeDirName = dirpath.split(os.path.sep)[-1]
            outPath = os.path.join(OUTPUT_DIR,timeDirName)
            outFile = os.path.join(os.path.join(OUTPUT_DIR,timeDirName), filename)
            # create PM/<date> directory
            if not os.path.isdir(outPath):
                os.mkdir(outPath)
                logger.info("create directory {dir}".format(dir=outPath))
            filePath = os.path.join(dirpath, filename)

            if filename.startswith("PM-MME") and filename.endswith("gz"):
                workDirWithTime = os.path.join(WORK_DIR, timeDirName)
                if not os.path.isdir(workDirWithTime):
                    os.mkdir(workDirWithTime)
                    logger.info("create directory {dir}".format(dir=workDirWithTime))
                workFilePath = os.path.join(os.path.join(WORK_DIR, timeDirName), filename)
                shutil.copy(filePath, workFilePath)
                logger.info("copied {srcFile} to {destFile}".format(srcFile=filePath, destFile=workFilePath))
                os.remove(filePath)
                logger.info("removed {sfile}".format(sfile=filePath))
                filePathList.append(workFilePath)
            elif filename.startswith("PM-") and filename.endswith("gz"):
                shutil.copy(filePath, outFile)
                logger.info("copied {srcFile} to {destFile}".format(srcFile=filePath, destFile=outFile))
                os.remove(filePath)
                logger.info("removed {sfile}".format(sfile=filePath))
            else:
                pass

    logger.info("ready to process {num} MME PM file: {pathList}".format(num=len(filePathList), pathList=filePathList))

    # Load factors.json
    factorsDict = {}
    with open(FACTORS_FILE, "r") as f:
        factorsDict = json.load(f)
    logger.debug("factors list: {f}".format(f=factorsDict))
    taList = factorsDict.keys()
    logger.debug("TA list: {ta}".format(ta=taList))

    # counters for compute SuccEpsAttachRate, PagingSuccRate, TauUpdateRate
    countersForComputeList = ["MM.SuccEpsAttach._Ta", "MM.AttEpsAttach._Ta", "MM.FailedEpsAttach._Ta.7.User", "MM.FailedEpsAttach._Ta.15.User", \
                              "MM.FailedEpsAttach._Ta.19.User", "MM.FirstPagingSucc._Ta","MM.SecondPagingSucc._Ta", "MM.PagAtt._Ta", \
                              "MM.TauAccept._Ta", "MM.TauRequest._Ta"]

    # counter name in factor.json mapping to CV N name
    counterCVNFirstHalfMappingDict = {"MM.SuccEpsAttach._Ta":"MM.SuccEpsAttach.",\
                                     "MM.AttEpsAttach._Ta":"MM.AttEpsAttach.", \
                                     "MM.FailedEpsAttach._Ta.7.User": "MM.FailedEpsAttach.", \
                                     "MM.FailedEpsAttach._Ta.15.User": "MM.FailedEpsAttach.", \
                                     "MM.FailedEpsAttach._Ta.19.User":"MM.FailedEpsAttach.", \
                                     "MM.FirstPagingSucc._Ta": "MM.FirstPagingSucc.", \
                                     "MM.SecondPagingSucc._Ta": "MM.SecondPagingSucc.", \
                                     "MM.PagAtt._Ta": "MM.PagAtt.", \
                                     "MM.TauAccept._Ta": "MM.TauAccept.", \
                                     "MM.TauRequest._Ta": "MM.TauRequest.", \
                                     }

    # Process
    for file in filePathList:
        logger.info("start processing {file}".format(file=file))
        fh = gzip.open(file, "r")
        try:
            content = fh.read()
        except Exception, e:
            logger.error(e)
        finally:
            fh.close()
        oldFile = file.replace(".gz", ".old")
        with open(oldFile, "w") as f:
            f.write(content)
        logger.info("generated {file}".format(file=oldFile))

        tree = ET.parse(oldFile)
        root = tree.getroot()
        logger.info("parsed {file}".format(file=oldFile))

        for ta in taList:
            logger.info("TA: {ta}".format(ta=ta))
            counters = factorsDict[ta][0].keys()
            counterIndexMappingDict = {}

            # Get Counter index number
            for N in root.getiterator("N"):
                if N.text in counters or N.text in countersForComputeList:
                    index = N.attrib["i"]
                    counterIndexMappingDict[N.text] = index
                    #logger.info("found counter: {counter}, index: {index}".format(counter=N.text, index=index))
            logger.info("counter index mapping: {map}".format(map=counterIndexMappingDict))

            for i in range(len(root)):
                Measurements = root[i]
                for j in range(len(Measurements)):
                    if root[i][j].tag == "ObjectType" and root[i][j].text == "MmeFunction":
                        MeasurementsLoc = i
                        logger.info("found ObjectType: {ot}, location: {loc}".format(ot=root[i][j].text, loc=MeasurementsLoc))
                        break
            for i in range(len(root[MeasurementsLoc])):
                if root[MeasurementsLoc][i].tag == "PmData":
                    PmDataLoc = i
                    logger.info("found PmData location: {loc}".format(loc=PmDataLoc))
                    break
            ObjectNum = len(root[MeasurementsLoc][PmDataLoc])
            logger.info("found total MME: {num}".format(num=ObjectNum))

            # Confirm factor whether cause over 100%
            countersForComputeSumDict = {}
            if not ta in countersForComputeSumDict.keys():
                countersForComputeSumDict[ta] = {}
            for counter in countersForComputeList:
                if counter not in countersForComputeSumDict[ta].keys():
                    countersForComputeSumDict[ta][counter] = 0
                sum = countersForComputeSumDict[ta][counter]
                for i in range(ObjectNum):
                    # Object
                    ObjectLoc = i
                    UserLabel = root[MeasurementsLoc][PmDataLoc][ObjectLoc].get("UserLabel")
                    for j in range(len(root[MeasurementsLoc][PmDataLoc][ObjectLoc])):
                        if root[MeasurementsLoc][PmDataLoc][ObjectLoc][j].get("i") == counterIndexMappingDict[counter]:
                            CVLoc = j
                            if counter in counterCVNFirstHalfMappingDict:
                                counterWithTAName = counterCVNFirstHalfMappingDict[counter] + ta
                                #logger.info("processing {counter} with TA: {cvn}".format(counter=counter, cvn=counterWithTAName))
                                for k in range(len(root[MeasurementsLoc][PmDataLoc][ObjectLoc][CVLoc])-1):
                                    SNSVLoc = k
                                    SN = root[MeasurementsLoc][PmDataLoc][ObjectLoc][CVLoc][SNSVLoc].text
                                    if SN == counterWithTAName:
                                        SV = int(root[MeasurementsLoc][PmDataLoc][ObjectLoc][CVLoc][SNSVLoc+1].text)
                                        old = sum
                                        sum = sum + SV
                                        countersForComputeSumDict[ta][counter] = sum
                                        logger.debug("{ta},{mme} {counter} {sn} {ovalue}+{sv} -> {nvalue}".format(ta=ta, mme=UserLabel, counter=counter, sn=SN, ovalue=old, sv=SV, nvalue=sum))
                                        #logger.info("found {mme} {counter} with value {value}".format(mme=UserLabel, counter=SN, value=SV))
                                    else:
                                        pass
                            else:
                                logger.error("{counter} Not in CV N First Half Mapping Directory".format(counter=counter))
                        else:
                            pass
            logger.info("sum up directory: {dict}".format(dict=countersForComputeSumDict))
            #countersForComputeSumDict_new = countersForComputeSumDict
            countersForCompute = countersForComputeSumDict[ta].keys()
            for countersForCompute in counters:
                old = countersForComputeSumDict[ta][countersForCompute]
                fact = float(factorsDict[ta][0][countersForCompute])
                countersForComputeSumDict[ta][countersForCompute] = countersForComputeSumDict[ta][countersForCompute] * fact
                logger.debug("{ta} {countersForCompute} {ovalue}*{fact} -> {nvalue}".format(ta=ta, countersForCompute=countersForCompute, ovalue=old, fact=fact, nvalue=countersForComputeSumDict[ta][countersForCompute]))
                #logger.debug("sum up directory change to: {dict}".format(dict=countersForComputeSumDict))
            logger.info("sum up directory change to: {dict}".format(dict=countersForComputeSumDict))

            # Caculate Rate
            try:
                # SuccEpsAttachRate
                if countersForComputeSumDict[ta]["MM.SuccEpsAttach._Ta"] == 0 and countersForComputeSumDict[ta]["MM.AttEpsAttach._Ta"] == 0:
                    SuccEpsAttachRate = 0
                    logger.info("{ta} SuccEpsAttachRate: 0, due to {c1} and {c2} is 0".format(ta=ta, c1="MM.SuccEpsAttach._Ta", c2="MM.AttEpsAttach._Ta"))
                else:
                    SuccEpsAttachRate = float(countersForComputeSumDict[ta]["MM.SuccEpsAttach._Ta"]) / float(countersForComputeSumDict[ta]["MM.AttEpsAttach._Ta"] - countersForComputeSumDict[ta]["MM.FailedEpsAttach._Ta.7.User"] - countersForComputeSumDict[ta]["MM.FailedEpsAttach._Ta.15.User"] - countersForComputeSumDict[ta]["MM.FailedEpsAttach._Ta.19.User"])
                    logger.info("{ta} SuccEpsAttachRate: {rate}".format(ta=ta, rate=SuccEpsAttachRate))
            except Exception, e:
                logger.error(e)
            try:
                # PagingSuccRate
                if countersForComputeSumDict[ta]["MM.PagAtt._Ta"] == 0:
                    PagingSuccRate = 0
                    logger.info("{ta} PagingSuccRate: 0, due to {counter} is 0".format(ta=ta, counter="MM.PagAtt._Ta"))
                else:
                    PagingSuccRate = float(countersForComputeSumDict[ta]["MM.FirstPagingSucc._Ta"] + countersForComputeSumDict[ta]["MM.SecondPagingSucc._Ta"]) / float(countersForComputeSumDict[ta]["MM.PagAtt._Ta"])
                    logger.info("{ta} PagingSuccRate: {rate}".format(ta=ta, rate=PagingSuccRate))
            except Exception, e:
                logger.error(e)
            try:
                # TauUpdateRate
                if countersForComputeSumDict[ta]["MM.TauRequest._Ta"] == 0:
                    TauUpdateRate = 0
                    logger.info("{ta} TauUpdateRate: 0, due to {counter} is 0".format(ta=ta, counter="MM.TauRequest._Ta"))
                else:
                    TauUpdateRate = float(countersForComputeSumDict[ta]["MM.TauAccept._Ta"]) / float(countersForComputeSumDict[ta]["MM.TauRequest._Ta"])
                    logger.info("{ta} TauUpdateRate: {rate}".format(ta=ta, rate=TauUpdateRate))
            except Exception, e:
                logger.error(e)

            # Adjust factor
            # SuccEpsAttachRate
            if SuccEpsAttachRate < 0:
                factorsDict[ta][0]["MM.SuccEpsAttach._Ta"] = 1
                factorsDict[ta][0]["MM.AttEpsAttach._Ta"] = 1
                logger.info("{ta} SuccEpsAttachRate < 0, set MM.SuccEpsAttach._Ta and MM.AttEpsAttach._Ta factor to 1".format(ta=ta))
                logger.debug("factors list change to: {f}".format(f=factorsDict[ta]))
            elif SuccEpsAttachRate > 1:
                SuccEpsAttachRate_new = random.uniform(SuccEpsAttachRate_Min, SuccEpsAttachRate_Max)
                logger.info("{ta} SuccEpsAttachRate > 1, decide SuccEpsAttachRate to {rate}".format(ta=ta, rate=SuccEpsAttachRate_new))
                factorsDict[ta][0]["MM.SuccEpsAttach._Ta"] = float(SuccEpsAttachRate_new * (countersForComputeSumDict[ta]["MM.AttEpsAttach._Ta"] - countersForComputeSumDict[ta]["MM.FailedEpsAttach._Ta.7.User"] - countersForComputeSumDict[ta]["MM.FailedEpsAttach._Ta.15.User"] - countersForComputeSumDict[ta]["MM.FailedEpsAttach._Ta.19.User"])) / float(countersForComputeSumDict[ta]["MM.SuccEpsAttach._Ta"]/factorsDict[ta][0]["MM.SuccEpsAttach._Ta"])
                factorsDict[ta][0]["MM.SuccEpsAttach._Ta"] = round(factorsDict[ta][0]["MM.SuccEpsAttach._Ta"], 6)
                logger.info("{ta} SuccEpsAttachRate > 1, set MM.SuccEpsAttach._Ta factor to {fact}".format(ta=ta, fact=factorsDict[ta][0]["MM.SuccEpsAttach._Ta"]))
                logger.debug("factors list change to: {f}".format(f=factorsDict[ta]))
            else:
                pass
            # PagingSuccRate
            if PagingSuccRate < 0:
                factorsDict[ta][0]["MM.FirstPagingSucc._Ta"] = 1
                factorsDict[ta][0]["MM.PagAtt._Ta"] = 1
                logger.info("{ta} PagingSuccRate < 0, set MM.FirstPagingSucc._Ta and MM.PagAtt._Ta factor to 1".format(ta=ta))
                logger.debug("factors list change to: {f}".format(f=factorsDict[ta]))
            elif PagingSuccRate > 1:
                PagingSuccRate_new = random.uniform(PagingSuccRate_Min, PagingSuccRate_Max)
                logger.info("{ta} PagingSuccRate > 1, decide PagingSuccRate to {rate}".format(ta=ta, rate=PagingSuccRate_new))
                factorsDict[ta][0]["MM.FirstPagingSucc._Ta"] = (PagingSuccRate_new * countersForComputeSumDict[ta]["MM.PagAtt._Ta"] - countersForComputeSumDict[ta]["MM.SecondPagingSucc._Ta"]) / (countersForComputeSumDict[ta]["MM.FirstPagingSucc._Ta"] / factorsDict[ta][0]["MM.FirstPagingSucc._Ta"])
                factorsDict[ta][0]["MM.FirstPagingSucc._Ta"] = round(factorsDict[ta][0]["MM.FirstPagingSucc._Ta"], 6)
                logger.info("{ta} PagingSuccRate > 1, set MM.FirstPagingSucc._Ta factor to {fact}".format(ta=ta, fact=factorsDict[ta][0]["MM.FirstPagingSucc._Ta"]))
                logger.debug("factors list change to: {f}".format(f=factorsDict[ta]))
            else:
                pass
            # TauUpdateRate
            if TauUpdateRate < 0:
                factorsDict[ta][0]["MM.TauAccept._Ta"] = 1
                logger.info("{ta} TauUpdateRate < 0, set MM.TauAccept._Ta factor to 1".format(ta=ta))
                logger.debug("factors list change to: {f}".format(f=factorsDict[ta]))
            elif TauUpdateRate > 1:
                TauUpdateRate_new = random.uniform(TauUpdateRate_Min, TauUpdateRate_Max)
                logger.info("{ta} TauUpdateRate > 1, decide TauUpdateRate to {rate}".format(ta=ta, rate=TauUpdateRate_new))
                factorsDict[ta][0]["MM.TauAccept._Ta"] = (TauUpdateRate_new * countersForComputeSumDict[ta]["MM.TauRequest._Ta"]) / (countersForComputeSumDict[ta]["MM.TauAccept._Ta"] / factorsDict[ta][0]["MM.TauAccept._Ta"])
                factorsDict[ta][0]["MM.TauAccept._Ta"] = round(factorsDict[ta][0]["MM.TauAccept._Ta"], 6)
                logger.info("{ta} TauUpdateRate > 1, set MM.TauAccept._Ta factor to {fact}".format(ta=ta, fact=factorsDict[ta][0]["MM.TauAccept._Ta"]))
                logger.debug("factors list change to: {f}".format(f=factorsDict[ta]))
            else:
                pass

            # Modify
            for counter in counters:
                for i in range(ObjectNum):
                    # Object
                    ObjectLoc = i
                    UserLabel = root[MeasurementsLoc][PmDataLoc][ObjectLoc].get("UserLabel")
                    for j in range(len(root[MeasurementsLoc][PmDataLoc][ObjectLoc])):
                        if root[MeasurementsLoc][PmDataLoc][ObjectLoc][j].get("i") == counterIndexMappingDict[counter]:
                            CVLoc = j
                            if counter in counterCVNFirstHalfMappingDict:
                                counterWithTAName = counterCVNFirstHalfMappingDict[counter] + ta
                                #logger.info("processing {counter} with TA: {cvn}".format(counter=counter, cvn=counterWithTAName))
                                for k in range(len(root[MeasurementsLoc][PmDataLoc][ObjectLoc][CVLoc])-1):
                                    SNSVLoc = k
                                    SN = root[MeasurementsLoc][PmDataLoc][ObjectLoc][CVLoc][SNSVLoc].text
                                    if SN == counterWithTAName:
                                        SV = float(root[MeasurementsLoc][PmDataLoc][ObjectLoc][CVLoc][SNSVLoc+1].text)
                                        #logger.info("found {mme} {counter} with value {value}".format(mme=UserLabel, counter=SN, value=SV))
                                        fact = float(factorsDict[ta][0][counter])
                                        SV_new = int(SV * fact)
                                        root[MeasurementsLoc][PmDataLoc][ObjectLoc][CVLoc][SNSVLoc+1].text = str(SV_new)
                                        logger.info("{mme} {sn} {value}*{fact} -> {value_new}".format(mme=UserLabel, sn=SN, value=SV, fact=fact, value_new=SV_new))
                                    else:
                                        pass
                            else:
                                logger.error("{counter} Not in CV N First Half Mapping Directory".format(counter=counter))
                        else:
                            pass

        newFile = file.replace(".gz",".new")
        tree.write(newFile, encoding="UTF-8")
        logger.info("write {file}".format(file=newFile))
        with open(newFile, "r") as f:
            content = f.readlines()
        for i in range(len(content)):
            if content[i].startswith("<PmFile"):
                content[i] = "<PmFile xmlns:xs=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">\n"
                logger.info("modified name space information")
                break
        with open(newFile, "w") as f:
            f.writelines(content)
        logger.info("generated {file}".format(file=newFile))

        with open(newFile, "r") as f:
            content = f.read()
        filename = os.path.basename(file)
        curdir = os.getcwd()
        finalOutDir = os.path.join(globVar.OUTPUT_DIR,file.split(os.path.sep)[-2])
        os.chdir(finalOutDir)
        fh = gzip.open(filename, 'w')
        try:
            fh.write(content)
            logger.info("generated {file}".format(file=os.path.join(finalOutDir, filename)))
        finally:
            os.chdir(curdir)
            fh.close()

    logger.info("END")

if __name__ == "__main__":
    beatify()