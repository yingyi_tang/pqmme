#!/usr/bin/env python
import os

# MAIN DIR
# /home/nmsadm/cadev/rru_status
BASE_DIR = os.path.split(os.path.realpath(__file__))[0]

# Directory
CONF_DIR = os.path.abspath(os.path.join(BASE_DIR, "..", "conf"))
INPUT_DIR = os.path.abspath(os.path.join(BASE_DIR, "..", "input"))
LOG_DIR = os.path.abspath(os.path.join(BASE_DIR, "..", "log"))
OUTPUT_DIR = os.path.abspath(os.path.join(BASE_DIR, "..", "output"))
#OUTPUT_DIR = "/opt/nbi/DataFile/GD/HX/ER/GZ_OMC1/PM"
TEMP_DIR = os.path.abspath(os.path.join(BASE_DIR, "..", "tmp"))
WORK_DIR = os.path.abspath(os.path.join(BASE_DIR, "..", "work"))
CSV_DIR = os.path.abspath(os.path.join(BASE_DIR, "..", "csv"))

# Files
LOG_FILE = os.path.abspath(os.path.join(LOG_DIR, "pqmme.log"))
LOGGER_FILE = os.path.abspath(os.path.join(CONF_DIR, "logger.conf"))
FACTORS_FILE = os.path.abspath(os.path.join(CONF_DIR, "factors.json"))

if __name__ == "__main__":
    print BASE_DIR
    print LOGGER_FILE
