import csv
import logging
import logging.config
import globVar
import datetime
import os
import shutil
import json
import xml.etree.ElementTree as ET

try:
    logging.config.fileConfig(globVar.LOGGER_FILE)
    logger = logging.getLogger("beautify")
except Exception, e:
    print(e)

WORK_DIR = globVar.WORK_DIR
FACTORS_FILE = globVar.FACTORS_FILE

"""
14,29,44,59 * * * * monitor.py
16:29 - p - 16:24/25 - PM-MME-A1-V3.0.0-20190110160000-15.xml.gz
16:44 - p - 16:39/40 - PM-MME-A1-V3.0.0-20190110161500-15.xml.gz
16:59 - p - 16:54/55 - PM-MME-A1-V3.0.0-20190110163000-15.xml.gz
17:14 - p - 17:09/10 - PM-MME-A1-V3.0.0-20190110164500-15.xml.gz

<nbi@gzltenbm01> </opt/nbi/DataFile/GD/HX/ER/GZ_OMC1/PM/2019011016> ls -lrth|grep MME
-rw-r--r-- 1 nbi nbi 1.5M Jan 10 16:24 PM-MME-A1-V3.0.0-20190110160000-15.xml.gz
-rw-r--r-- 1 nbi nbi 1.5M Jan 10 16:39 PM-MME-A1-V3.0.0-20190110161500-15.xml.gz
-rw-r--r-- 1 nbi nbi 1.5M Jan 10 16:54 PM-MME-A1-V3.0.0-20190110163000-15.xml.gz
-rw-r--r-- 1 nbi nbi 1.5M Jan 10 17:09 PM-MME-A1-V3.0.0-20190110164500-15.xml.gz
"""

def mon():
    logger.info("START")

    # Find MME PM files
    filePathList = []
    now = datetime.datetime.now()
    cur_day = now.strftime("%d")
    cur_date = now.strftime("%Y%m") + cur_day
    cur_min = now.strftime("%M")
    cur_hour = now.strftime("%H")
    cur_sec = "00"
    filename_min = cur_min
    if cur_min == "29":
        filename_min = "00"
    elif cur_min == "44":
        filename_min = "15"
    elif cur_min == "59":
        filename_min = "30"
    elif cur_min == "14":
        filename_min = "45"
    else:
        pass

    filename_hour = cur_hour
    folder_hour = cur_hour
    delta = datetime.timedelta(hours=-1)
    if cur_min == "14":
        filename_hour = (now + delta).strftime("%H")
        folder_hour = (now + delta).strftime("%H")
        if cur_hour == "00":
            delta_day = datetime.timedelta(days=-1)
            cur_day = (now + delta_day).strftime("%d")
            cur_date = now.strftime("%Y%m") + cur_day
    else:
        pass

    directory = "/opt/nbi/DataFile/pqmme/work/" + cur_date + folder_hour

    # Prccess NEW file
    filename = "PM-MME-A1-V3.0.0-" + cur_date + filename_hour + filename_min + cur_sec + "-15.xml.new"
    #filename = "C:\\Users\\eyiitag\\Documents\\pqmme\\work\\2019020410\\PM-MME-A1-V3.0.0-20190204104500-15.xml.new"
    file = os.path.join(directory, filename)
    if os.path.isfile(file):
        logger.info("found {file}".format(file=file))
    else:
        logger.info("missing {file}".format(file=file))
        logger.info("END")
        exit(1)

    logger.info("ready to process MME PM file: {file}".format(file=file))

    with open(FACTORS_FILE, "r") as f:
        factorsDict = json.load(f)
    logger.debug("factors list: {f}".format(f=factorsDict))
    taList = factorsDict.keys()
    logger.debug("TA list: {ta}".format(ta=taList))

    # counters for compute SuccEpsAttachRate, PagingSuccRate, TauUpdateRate
    countersForComputeList = ["MM.SuccEpsAttach._Ta", "MM.AttEpsAttach._Ta", "MM.FailedEpsAttach._Ta.7.User", "MM.FailedEpsAttach._Ta.15.User", \
                              "MM.FailedEpsAttach._Ta.19.User", "MM.FirstPagingSucc._Ta","MM.SecondPagingSucc._Ta", "MM.PagAtt._Ta", \
                              "MM.TauAccept._Ta", "MM.TauRequest._Ta"]

    # counter name in factor.json mapping to CV N name
    counterCVNFirstHalfMappingDict = {"MM.SuccEpsAttach._Ta":"MM.SuccEpsAttach.",\
                                     "MM.AttEpsAttach._Ta":"MM.AttEpsAttach.", \
                                     "MM.FailedEpsAttach._Ta.7.User": "MM.FailedEpsAttach.", \
                                     "MM.FailedEpsAttach._Ta.15.User": "MM.FailedEpsAttach.", \
                                     "MM.FailedEpsAttach._Ta.19.User":"MM.FailedEpsAttach.", \
                                     "MM.FirstPagingSucc._Ta": "MM.FirstPagingSucc.", \
                                     "MM.SecondPagingSucc._Ta": "MM.SecondPagingSucc.", \
                                     "MM.PagAtt._Ta": "MM.PagAtt.", \
                                     "MM.TauAccept._Ta": "MM.TauAccept.", \
                                     "MM.TauRequest._Ta": "MM.TauRequest.", \
                                     }

    tree = ET.parse(file)
    root = tree.getroot()
    logger.info("parsed {file}".format(file=file))

    for ta in taList:
        logger.info("TA: {ta}".format(ta=ta))
        counters = factorsDict[ta][0].keys()
        counterIndexMappingDict = {}

        # Get Counter index number
        for N in root.getiterator("N"):
            if N.text in counters or N.text in countersForComputeList:
                index = N.attrib["i"]
                counterIndexMappingDict[N.text] = index
                #logger.info("found counter: {counter}, index: {index}".format(counter=N.text, index=index))
        logger.info("counter index mapping: {map}".format(map=counterIndexMappingDict))

        for i in range(len(root)):
            Measurements = root[i]
            for j in range(len(Measurements)):
                if root[i][j].tag == "ObjectType" and root[i][j].text == "MmeFunction":
                    MeasurementsLoc = i
                    #logger.info("found ObjectType: {ot}, location: {loc}".format(ot=root[i][j].text, loc=MeasurementsLoc))
                    break
        for i in range(len(root[MeasurementsLoc])):
            if root[MeasurementsLoc][i].tag == "PmData":
                PmDataLoc = i
                #logger.info("found PmData location: {loc}".format(loc=PmDataLoc))
                break
        ObjectNum = len(root[MeasurementsLoc][PmDataLoc])
        #logger.info("found total MME: {num}".format(num=ObjectNum))

        # Confirm factor whether cause over 100%
        csvFile = os.path.join(globVar.CSV_DIR, "PM-MME-A1-V3.0.0-" + cur_date + ".upg.csv")
        csvTime = filename_hour + ":" + filename_min
        countersForComputeSumDict = {}
        if not ta in countersForComputeSumDict.keys():
            countersForComputeSumDict[ta] = {}
        for counter in countersForComputeList:
            if counter not in countersForComputeSumDict[ta].keys():
                countersForComputeSumDict[ta][counter] = 0
            sum = countersForComputeSumDict[ta][counter]
            for i in range(ObjectNum):
                # Object
                ObjectLoc = i
                UserLabel = root[MeasurementsLoc][PmDataLoc][ObjectLoc].get("UserLabel")
                for j in range(len(root[MeasurementsLoc][PmDataLoc][ObjectLoc])):
                    if root[MeasurementsLoc][PmDataLoc][ObjectLoc][j].get("i") == counterIndexMappingDict[counter]:
                        CVLoc = j
                        if counter in counterCVNFirstHalfMappingDict:
                            counterWithTAName = counterCVNFirstHalfMappingDict[counter] + ta
                            #logger.info("processing {counter} with TA: {cvn}".format(counter=counter, cvn=counterWithTAName))
                            for k in range(len(root[MeasurementsLoc][PmDataLoc][ObjectLoc][CVLoc])-1):
                                SNSVLoc = k
                                SN = root[MeasurementsLoc][PmDataLoc][ObjectLoc][CVLoc][SNSVLoc].text
                                if SN == counterWithTAName:
                                    SV = int(root[MeasurementsLoc][PmDataLoc][ObjectLoc][CVLoc][SNSVLoc+1].text)
                                    old = sum
                                    sum = sum + SV
                                    countersForComputeSumDict[ta][counter] = sum
                                    if counter in counters:
                                        writeCsv(csvFile, cur_date, csvTime, ta, UserLabel, counter, SV)
                                    #logger.debug("{ta},{mme} {counter} {sn} {ovalue}+{sv} -> {nvalue}".format(ta=ta, mme=UserLabel, counter=counter, sn=SN, ovalue=old, sv=SV, nvalue=sum))
                                    #logger.info("found {mme} {counter} with value {value}".format(mme=UserLabel, counter=SN, value=SV))
                                else:
                                    pass
                        else:
                            logger.error("{counter} Not in CV N First Half Mapping Directory".format(counter=counter))
                    else:
                        pass
        logger.info("sum up directory: {dict}".format(dict=countersForComputeSumDict))
        """
        #countersForComputeSumDict_new = countersForComputeSumDict
        countersForCompute = countersForComputeSumDict[ta].keys()
        for countersForCompute in counters:
            old = countersForComputeSumDict[ta][countersForCompute]
            fact = float(factorsDict[ta][0][countersForCompute])
            countersForComputeSumDict[ta][countersForCompute] = countersForComputeSumDict[ta][countersForCompute] * fact
            logger.debug("{ta} {countersForCompute} {ovalue}*{fact} -> {nvalue}".format(ta=ta, countersForCompute=countersForCompute, ovalue=old, fact=fact, nvalue=countersForComputeSumDict[ta][countersForCompute]))
            #logger.debug("sum up directory change to: {dict}".format(dict=countersForComputeSumDict))
        logger.info("sum up directory change to: {dict}".format(dict=countersForComputeSumDict))
        """
        # Caculate Rate
        try:
            # SuccEpsAttachRate
            if countersForComputeSumDict[ta]["MM.SuccEpsAttach._Ta"] == 0 and countersForComputeSumDict[ta]["MM.AttEpsAttach._Ta"] == 0:
                SuccEpsAttachRate = 0
                logger.info("{ta} SuccEpsAttachRate: 0, due to {c1} and {c2} is 0".format(ta=ta, c1="MM.SuccEpsAttach._Ta", c2="MM.AttEpsAttach._Ta"))
            else:
                SuccEpsAttachRate = float(countersForComputeSumDict[ta]["MM.SuccEpsAttach._Ta"]) / float(countersForComputeSumDict[ta]["MM.AttEpsAttach._Ta"] - countersForComputeSumDict[ta]["MM.FailedEpsAttach._Ta.7.User"] - countersForComputeSumDict[ta]["MM.FailedEpsAttach._Ta.15.User"] - countersForComputeSumDict[ta]["MM.FailedEpsAttach._Ta.19.User"])
                logger.info("{ta} SuccEpsAttachRate: {rate}".format(ta=ta, rate=SuccEpsAttachRate))
        except Exception, e:
            logger.error(e)
        try:
            # PagingSuccRate
            if countersForComputeSumDict[ta]["MM.PagAtt._Ta"] == 0:
                PagingSuccRate = 0
                logger.info("{ta} PagingSuccRate: 0, due to {counter} is 0".format(ta=ta, counter="MM.PagAtt._Ta"))
            else:
                PagingSuccRate = float(countersForComputeSumDict[ta]["MM.FirstPagingSucc._Ta"] + countersForComputeSumDict[ta]["MM.SecondPagingSucc._Ta"]) / float(countersForComputeSumDict[ta]["MM.PagAtt._Ta"])
                logger.info("{ta} PagingSuccRate: {rate}".format(ta=ta, rate=PagingSuccRate))
        except Exception, e:
            logger.error(e)
        try:
            # TauUpdateRate
            if countersForComputeSumDict[ta]["MM.TauRequest._Ta"] == 0:
                TauUpdateRate = 0
                logger.info("{ta} TauUpdateRate: 0, due to {counter} is 0".format(ta=ta, counter="MM.TauRequest._Ta"))
            else:
                TauUpdateRate = float(countersForComputeSumDict[ta]["MM.TauAccept._Ta"]) / float(countersForComputeSumDict[ta]["MM.TauRequest._Ta"])
                logger.info("{ta} TauUpdateRate: {rate}".format(ta=ta, rate=TauUpdateRate))
        except Exception, e:
            logger.error(e)

        #csvFile = "PM-MME-A1-V3.0.0-" + cur_date + ".csv"
        csvRateFile = os.path.join(globVar.CSV_DIR,"PM-MME-A1-V3.0.0-" + cur_date + ".rate.upg.csv")
        #time = cur_hour + ":" + cur_min
        time = filename_hour + ":" + filename_min
        if os.path.isfile(csvRateFile):
            writeRateCsv(csvRateFile, cur_date, time, ta, SuccEpsAttachRate, PagingSuccRate, TauUpdateRate)
            logger.info("write line: {day},{time},{tac},{SuccEpsAttachRate},{PagingSuccRate},{TauUpdateRate}".format( \
                day=cur_date, time=time, tac=ta, SuccEpsAttachRate=SuccEpsAttachRate, PagingSuccRate=PagingSuccRate, TauUpdateRate=TauUpdateRate))
        else:
            writeRateCsv(csvRateFile, "date", "time", "tac", "SuccEpsAttachRate", "PagingSuccRate", "TauUpdateRate")
            writeRateCsv(csvRateFile, cur_date, time, ta, SuccEpsAttachRate, PagingSuccRate, TauUpdateRate)
            logger.info("write line: {day},{time},{tac},{SuccEpsAttachRate},{PagingSuccRate},{TauUpdateRate}".format( \
                day=cur_date, time=time, tac=ta, SuccEpsAttachRate=SuccEpsAttachRate, PagingSuccRate=PagingSuccRate, TauUpdateRate=TauUpdateRate))

    #Process OLD filw
    filename = "PM-MME-A1-V3.0.0-" + cur_date + filename_hour + filename_min + cur_sec + "-15.xml.old"
    file = os.path.join(directory, filename)
    if os.path.isfile(file):
        logger.info("found {file}".format(file=file))
    else:
        logger.info("missing {file}".format(file=file))
        logger.info("END")
        exit(1)

    logger.info("ready to process MME PM file: {file}".format(file=file))

    with open(FACTORS_FILE, "r") as f:
        factorsDict = json.load(f)
    logger.debug("factors list: {f}".format(f=factorsDict))
    taList = factorsDict.keys()
    logger.debug("TA list: {ta}".format(ta=taList))

    # counters for compute SuccEpsAttachRate, PagingSuccRate, TauUpdateRate
    countersForComputeList = ["MM.SuccEpsAttach._Ta", "MM.AttEpsAttach._Ta", "MM.FailedEpsAttach._Ta.7.User",
                              "MM.FailedEpsAttach._Ta.15.User", \
                              "MM.FailedEpsAttach._Ta.19.User", "MM.FirstPagingSucc._Ta", "MM.SecondPagingSucc._Ta",
                              "MM.PagAtt._Ta", \
                              "MM.TauAccept._Ta", "MM.TauRequest._Ta"]

    # counter name in factor.json mapping to CV N name
    counterCVNFirstHalfMappingDict = {"MM.SuccEpsAttach._Ta": "MM.SuccEpsAttach.", \
                                      "MM.AttEpsAttach._Ta": "MM.AttEpsAttach.", \
                                      "MM.FailedEpsAttach._Ta.7.User": "MM.FailedEpsAttach.", \
                                      "MM.FailedEpsAttach._Ta.15.User": "MM.FailedEpsAttach.", \
                                      "MM.FailedEpsAttach._Ta.19.User": "MM.FailedEpsAttach.", \
                                      "MM.FirstPagingSucc._Ta": "MM.FirstPagingSucc.", \
                                      "MM.SecondPagingSucc._Ta": "MM.SecondPagingSucc.", \
                                      "MM.PagAtt._Ta": "MM.PagAtt.", \
                                      "MM.TauAccept._Ta": "MM.TauAccept.", \
                                      "MM.TauRequest._Ta": "MM.TauRequest.", \
                                      }

    tree = ET.parse(file)
    root = tree.getroot()
    logger.info("parsed {file}".format(file=file))

    for ta in taList:
        logger.info("TA: {ta}".format(ta=ta))
        counters = factorsDict[ta][0].keys()
        counterIndexMappingDict = {}

        # Get Counter index number
        for N in root.getiterator("N"):
            if N.text in counters or N.text in countersForComputeList:
                index = N.attrib["i"]
                counterIndexMappingDict[N.text] = index
                # logger.info("found counter: {counter}, index: {index}".format(counter=N.text, index=index))
        logger.info("counter index mapping: {map}".format(map=counterIndexMappingDict))

        for i in range(len(root)):
            Measurements = root[i]
            for j in range(len(Measurements)):
                if root[i][j].tag == "ObjectType" and root[i][j].text == "MmeFunction":
                    MeasurementsLoc = i
                    # logger.info("found ObjectType: {ot}, location: {loc}".format(ot=root[i][j].text, loc=MeasurementsLoc))
                    break
        for i in range(len(root[MeasurementsLoc])):
            if root[MeasurementsLoc][i].tag == "PmData":
                PmDataLoc = i
                # logger.info("found PmData location: {loc}".format(loc=PmDataLoc))
                break
        ObjectNum = len(root[MeasurementsLoc][PmDataLoc])
        # logger.info("found total MME: {num}".format(num=ObjectNum))

        # Confirm factor whether cause over 100%
        csvFile = os.path.join(globVar.CSV_DIR, "PM-MME-A1-V3.0.0-" + cur_date + ".ori.csv")
        csvTime = filename_hour + ":" + filename_min
        countersForComputeSumDict = {}
        if not ta in countersForComputeSumDict.keys():
            countersForComputeSumDict[ta] = {}
        for counter in countersForComputeList:
            if counter not in countersForComputeSumDict[ta].keys():
                countersForComputeSumDict[ta][counter] = 0
            sum = countersForComputeSumDict[ta][counter]
            for i in range(ObjectNum):
                # Object
                ObjectLoc = i
                UserLabel = root[MeasurementsLoc][PmDataLoc][ObjectLoc].get("UserLabel")
                for j in range(len(root[MeasurementsLoc][PmDataLoc][ObjectLoc])):
                    if root[MeasurementsLoc][PmDataLoc][ObjectLoc][j].get("i") == counterIndexMappingDict[counter]:
                        CVLoc = j
                        if counter in counterCVNFirstHalfMappingDict:
                            counterWithTAName = counterCVNFirstHalfMappingDict[counter] + ta
                            # logger.info("processing {counter} with TA: {cvn}".format(counter=counter, cvn=counterWithTAName))
                            for k in range(len(root[MeasurementsLoc][PmDataLoc][ObjectLoc][CVLoc]) - 1):
                                SNSVLoc = k
                                SN = root[MeasurementsLoc][PmDataLoc][ObjectLoc][CVLoc][SNSVLoc].text
                                if SN == counterWithTAName:
                                    SV = int(root[MeasurementsLoc][PmDataLoc][ObjectLoc][CVLoc][SNSVLoc + 1].text)
                                    old = sum
                                    sum = sum + SV
                                    countersForComputeSumDict[ta][counter] = sum
                                    if counter in counters:
                                        writeCsv(csvFile, cur_date, csvTime, ta, UserLabel, counter, SV)
                                    # logger.debug("{ta},{mme} {counter} {sn} {ovalue}+{sv} -> {nvalue}".format(ta=ta, mme=UserLabel, counter=counter, sn=SN, ovalue=old, sv=SV, nvalue=sum))
                                    # logger.info("found {mme} {counter} with value {value}".format(mme=UserLabel, counter=SN, value=SV))
                                else:
                                    pass
                        else:
                            logger.error(
                                "{counter} Not in CV N First Half Mapping Directory".format(counter=counter))
                    else:
                        pass
        logger.info("sum up directory: {dict}".format(dict=countersForComputeSumDict))
        """
        #countersForComputeSumDict_new = countersForComputeSumDict
        countersForCompute = countersForComputeSumDict[ta].keys()
        for countersForCompute in counters:
            old = countersForComputeSumDict[ta][countersForCompute]
            fact = float(factorsDict[ta][0][countersForCompute])
            countersForComputeSumDict[ta][countersForCompute] = countersForComputeSumDict[ta][countersForCompute] * fact
            logger.debug("{ta} {countersForCompute} {ovalue}*{fact} -> {nvalue}".format(ta=ta, countersForCompute=countersForCompute, ovalue=old, fact=fact, nvalue=countersForComputeSumDict[ta][countersForCompute]))
            #logger.debug("sum up directory change to: {dict}".format(dict=countersForComputeSumDict))
        logger.info("sum up directory change to: {dict}".format(dict=countersForComputeSumDict))
        """
        # Caculate Rate
        try:
            # SuccEpsAttachRate
            if countersForComputeSumDict[ta]["MM.SuccEpsAttach._Ta"] == 0 and countersForComputeSumDict[ta][
                "MM.AttEpsAttach._Ta"] == 0:
                SuccEpsAttachRate = 0
                logger.info(
                    "{ta} SuccEpsAttachRate: 0, due to {c1} and {c2} is 0".format(ta=ta, c1="MM.SuccEpsAttach._Ta",
                                                                                  c2="MM.AttEpsAttach._Ta"))
            else:
                SuccEpsAttachRate = float(countersForComputeSumDict[ta]["MM.SuccEpsAttach._Ta"]) / float(
                    countersForComputeSumDict[ta]["MM.AttEpsAttach._Ta"] - countersForComputeSumDict[ta][
                        "MM.FailedEpsAttach._Ta.7.User"] - countersForComputeSumDict[ta][
                        "MM.FailedEpsAttach._Ta.15.User"] - countersForComputeSumDict[ta][
                        "MM.FailedEpsAttach._Ta.19.User"])
                logger.info("{ta} SuccEpsAttachRate: {rate}".format(ta=ta, rate=SuccEpsAttachRate))
        except Exception, e:
            logger.error(e)
        try:
            # PagingSuccRate
            if countersForComputeSumDict[ta]["MM.PagAtt._Ta"] == 0:
                PagingSuccRate = 0
                logger.info("{ta} PagingSuccRate: 0, due to {counter} is 0".format(ta=ta, counter="MM.PagAtt._Ta"))
            else:
                PagingSuccRate = float(
                    countersForComputeSumDict[ta]["MM.FirstPagingSucc._Ta"] + countersForComputeSumDict[ta][
                        "MM.SecondPagingSucc._Ta"]) / float(countersForComputeSumDict[ta]["MM.PagAtt._Ta"])
                logger.info("{ta} PagingSuccRate: {rate}".format(ta=ta, rate=PagingSuccRate))
        except Exception, e:
            logger.error(e)
        try:
            # TauUpdateRate
            if countersForComputeSumDict[ta]["MM.TauRequest._Ta"] == 0:
                TauUpdateRate = 0
                logger.info(
                    "{ta} TauUpdateRate: 0, due to {counter} is 0".format(ta=ta, counter="MM.TauRequest._Ta"))
            else:
                TauUpdateRate = float(countersForComputeSumDict[ta]["MM.TauAccept._Ta"]) / float(
                    countersForComputeSumDict[ta]["MM.TauRequest._Ta"])
                logger.info("{ta} TauUpdateRate: {rate}".format(ta=ta, rate=TauUpdateRate))
        except Exception, e:
            logger.error(e)

        # csvFile = "PM-MME-A1-V3.0.0-" + cur_date + ".csv"
        csvRateFile = os.path.join(globVar.CSV_DIR, "PM-MME-A1-V3.0.0-" + cur_date + ".rate.ori.csv")
        # time = cur_hour + ":" + cur_min
        time = filename_hour + ":" + filename_min
        if os.path.isfile(csvRateFile):
            writeRateCsv(csvRateFile, cur_date, time, ta, SuccEpsAttachRate, PagingSuccRate, TauUpdateRate)
            logger.info(
                "write line: {day},{time},{tac},{SuccEpsAttachRate},{PagingSuccRate},{TauUpdateRate}".format( \
                    day=cur_date, time=time, tac=ta, SuccEpsAttachRate=SuccEpsAttachRate,
                    PagingSuccRate=PagingSuccRate, TauUpdateRate=TauUpdateRate))
        else:
            writeRateCsv(csvRateFile, "date", "time", "tac", "SuccEpsAttachRate", "PagingSuccRate", "TauUpdateRate")
            writeRateCsv(csvRateFile, cur_date, time, ta, SuccEpsAttachRate, PagingSuccRate, TauUpdateRate)
            logger.info(
                "write line: {day},{time},{tac},{SuccEpsAttachRate},{PagingSuccRate},{TauUpdateRate}".format( \
                    day=cur_date, time=time, tac=ta, SuccEpsAttachRate=SuccEpsAttachRate,
                    PagingSuccRate=PagingSuccRate, TauUpdateRate=TauUpdateRate))

    logger.info("END")

def writeRateCsv(csvfile, date, time, tac, SuccEpsAttachRate, PagingSuccRate, TauUpdateRate):
    csvLine = [date, time, tac, SuccEpsAttachRate, PagingSuccRate, TauUpdateRate]
    if not os.path.isfile(csvfile):
        f = open(csvfile, "w")
        f.close()
    csvFile = file(csvfile, "ab")
    writter = csv.writer(csvFile)
    writter.writerow(csvLine)
    csvFile.close()

def writeCsv(csvfile, date, time, tac, mme, counterName, value):
    csvLine = [date, time, tac, mme, counterName, value]
    if not os.path.isfile(csvfile):
        f = open(csvfile, "w")
        f.close()
        csvFile = file(csvfile, "ab")
        writter = csv.writer(csvFile)
        writter.writerow(["date", "time", "tac", "mme", "counterName", "value"])
        csvFile.close()
    csvFile = file(csvfile, "ab")
    writter = csv.writer(csvFile)
    writter.writerow(csvLine)
    csvFile.close()

if __name__ == "__main__":
    mon()

