
import globVar
import commands
import logging
import logging.config
import os

try:
    logging.config.fileConfig(globVar.LOGGER_FILE)
    logger = logging.getLogger("beautify")
except Exception, e:
    print(e)

def housekeeping():
    try:
        cmd = "/bin/find {csvDir}/ -mtime 3 -type f -exec rm {char} \;".format(csvDir=globVar.CSV_DIR, char="{}")
        status, ret = commands.getstatusoutput(cmd)
        if status==0:
            logger.info("run {cmd}".format(cmd=cmd))
        else:
            logger.info("run {cmd} failed".format(cmd=cmd))
            """
        cmd = "/bin/find {link_dir}/ -mtime 1 -type f -exec rm {char} \;".format(link_dir=link_dir, char="{}")
        status, ret = commands.getstatusoutput(cmd)
        if status==0:
            logger.info("run {cmd}".format(cmd=cmd))
        else:
            logger.info("run {cmd} failed".format(cmd=cmd))
        """
        # M
        fsize = os.path.getsize(globVar.LOG_FILE)/1024/2014
        if fsize >= 50:
            os.remove(globVar.LOG_FILE)
            logger.info("log file size {size}, remove {file}".format(size=fsize, file=globVar.LOG_FILE))
    except Exception, e:
        logger.error(e)

if __name__ == "__main__":
    housekeeping()
